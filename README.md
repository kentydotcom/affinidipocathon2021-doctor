# Affinidi POCathon Health Insurance Use Case Implementation - Doctor - Issuer

## Introduction

Welcome to the Doctor Portal for the Health Insurance Use Case Implementation designed by Dr Kent Lau [https://linkedin.com/in/kentglau].

<br/>
<br/>
<img src="./src/assets/images/icons/medical-portal.png">